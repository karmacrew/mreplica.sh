#!/usr/bin/env sh

VER=1.0

PROJECT_NAME="mreplicaiset.sh"

###
# Install mongoDB and packages
###

#Import key
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

#Create list file
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
#Reload package list
sudo apt-get update

#Install MondoDB packages
sudo apt-get install -y mongodb-org=3.6.2 mongodb-org-server=3.6.2 mongodb-org-shell=3.6.2 mongodb-org-mongos=3.6.2 mongodb-org-tools=3.6.2

#Pin version
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections

###
# Config replica set member
##

#Generate keyFile
openssl rand -base64 756 > /var/lib/mongodb/keyfile
chmod 400 /var/lib/mongodb/keyfile
chown mongodb:mongodb /var/lib/mongodb/keyfile

# Set use keyFile and bind all
echo '# mongod.conf

# for documentation of all options, see:
#   http://docs.mongodb.org/manual/reference/configuration-options/

# Where and how to store data.
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
#  engine:
#  mmapv1:
#  wiredTiger:

# where to write logging data.
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log

# network interfaces
net:
  port: 27017
  bindIpAll: true
# how the process runs
processManagement:
  timeZoneInfo: /usr/share/zoneinfo

security:
  keyFile: /var/lib/mongodb/keyfile
#operationProfiling:

replication:
  replSetName: replicaSetOne

#sharding:

## Enterprise-Only Options:

#auditLog:

#snmp:
' > /etc/mongod.conf
